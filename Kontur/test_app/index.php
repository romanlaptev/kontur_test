<?php
error_reporting(E_ALL|E_STRICT);
ini_set("display_errors", true);


//=============================== check PHP version
$required_ver = '5.1.0';
$res = version_compare( PHP_VERSION,  $required_ver );
//echo "version_compare: ". $res;
//echo "<br/>\n";

switch( $res ){

	case -1:
		$msg = "Your PHP version ".PHP_VERSION." < ".$required_ver;
		echo _logWrap($msg, "info");

		$msg = "error, could not run application...";
		echo _logWrap($msg, "error");

		$msg = "<a href='../frameworks/yii/requirements/'>look at the minimal requirements here...</a>";
		echo _logWrap($msg, "info");

		//$loadedExt = get_loaded_extensions();
		//echo _logWrap( $loadedExt, "info");

		exit;
	break;
/*
	case 0:
echo "Your PHP version === ".$required_ver ;
echo "<br/>\n";
	break;

	case 1:
echo "Your PHP version > ".$required_ver ;
echo "<br/>\n";
	break;
*/
}//end switch

require_once "app.php";

//============================
function _logWrap( $msg, $level = "info"){

	// check API type
	$sapi_type = php_sapi_name();
//echo "php_sapi_name: ". $sapi_type;
//echo "<br/>\n";
//echo "type: ". gettype( $msg);
//echo "<br/>\n";

//-------------
	//$runType = "";
	//if ( $sapi_type == 'apache2handler' || 
			//$sapi_type == 'cli-server'
		//) {
		$runType = "web";
	//}

	if ( $sapi_type == "cli" ) { $runType = "console"; }
	if ( $sapi_type == "cgi" ) { $runType = "console"; }

//-------------
	if( gettype( $msg) === "array" || 
		gettype( $msg) === "object"
	){
			if ( $runType == "web" ) {
				$out = "<pre>".print_r($msg,1)."</pre>";
				return $out;
			} else {
				$out = print_r($msg,1)."\n";
				return $out;
			}
	}

	if( gettype( $msg) !== "string"){
		return false;
	}

//-------------
	switch ($level) {
		case "info":
			if ( $runType == "web" ) {
				return "<div class='alert alert-info'>".$msg."</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "warning":
			if ( $runType == "web" ) {
				return "<div class='alert alert-warning'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "danger":
		case "error":
			if ( $runType == "web" ) {
				return "<div class='alert alert-danger'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "success":
			if ( $runType == "web" ) {
				return "<div class='alert alert-success'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		default:
			if ( $runType == "web" ) {
				return $msg. "<br/>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
	}//end switch

}//end _logWrap()



