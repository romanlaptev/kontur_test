<?php
runApp();

//---------------------------
function runApp(){

// change the following paths if necessary
$yii=dirname(__FILE__).'/../frameworks/yii/framework/yii.php';

//$config=dirname(__FILE__).'/protected/config/main.php';
//if( $_SERVER["HTTP_HOST"] === "vbox1") {
	$config=dirname(__FILE__).'/protected/config/main_local.php';
//}
require_once '../libraries/PHPExcel/IOFactory.php';

$res = checkDb( $config );
if( !$res ){
	$msg = "error, wrong database";
	$msg .= ", could not run application...";
	echo _logWrap( $msg, "error" );
	return;
}

//--------------------------------
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();

}//end runApp()


//---------------------------
function checkDb( $config=null ){

	$res = false;
	
	if (is_string($config) ){
		$conf = require($config);
	}
//echo _logWrap( $conf["components"]["db"] );

//---------------------------
/*
$moduleName = "sqlite3";
$loadedExt = get_loaded_extensions();
if ( !in_array( $moduleName, $loadedExt ) ) {
	$msg = "<p>-- error, $moduleName module  is not in the list of loaded extensions...</p>";
	echo $msg;
echo "loaded_extensions:<pre>";
print_r( $loadedExt );
echo "</pre>";
	exit;
}
*/

/*
	$db_class_path=dirname(__FILE__).'/../frameworks/yii/framework/base/interfaces.php';
	require( $db_class_path );

	$db_class_path=dirname(__FILE__).'/../frameworks/yii/framework/base/CComponent.php';
	require( $db_class_path );

	$db_class_path=dirname(__FILE__).'/../frameworks/yii/framework/base/CApplicationComponent.php';
	require( $db_class_path );
	
	$db_class_path=dirname(__FILE__).'/../frameworks/yii/framework/db/CDbConnection.php';
	require( $db_class_path );

	$dsn="mysql:host=localhost";
	$user="root";
	$password="master";
	$connection = new CDbConnection( $dsn, $user, $password );
//echo _logWrap( $connection );
*/

    
	//$dbHost = "localhost";
	//$dbUser = "root";
	//$dbPassword = "master";
	//$dsn = "mysql:host={$dbHost}";
	
	//check  file SQLITE database
	$pos = strpos( $conf["components"]["db"]["connectionString"], "sqlite");
// 	echo "pos:". $pos.", type:". gettype($pos);
// 	echo "<br>\n";
// 	
// 	echo "pos === false:";
// 	echo $pos === false;
// 	echo "<br>\n";
	if( $pos !== false ){
	
		//[connectionString] => sqlite:/mnt/serv_d1/www/projects/Kontur_test/test_app/protected/config/../data/db1.sqlite		    
		$split_arr = explode( ":", $conf["components"]["db"]["connectionString"]);
		$filePath = $split_arr[1];
 		if ( file_exists( $filePath ) )	{
 			return true;
 		} else {
 			$msg = "error, not found database: ".$filePath;
 			echo _logWrap( $msg, "error");
 			return false;
 		}
		
	}

	
	//check MySql server
	$pos = strpos( $conf["components"]["db"]["connectionString"], "mysql");
	if( $pos !== false ){
		return checkDbMysql( $conf["components"]["db"] );
	}
	
	return $res;
	
}//end checkDb()


function checkDbMySql( $config ){

	$split_arr = explode( ";", $config["connectionString"] );// mysql:host=localhost;dbname=db1
	$dsn = $split_arr[0];// mysql:host=localhost
	
	$dbName = explode( "=", $split_arr[1] )[1];//db1
//echo _logWrap( "dbName: ". $dbName );
	
	$dbUser = $config["username"];
	$dbPassword = $config["password"];
	
	try{
		$connection = new PDO( $dsn, $dbUser, $dbPassword );

		//$msg =  "-- ok, connected to the server <b>".$dsn."</b> as ";
		//$msg .=  "<b> ".$dbUser."</b>";
		//echo _logWrap( $msg, "success" );

/*		
		$sql_query = "SHOW DATABASES";
		$result  = $connection->query( $sql_query );
		if( !$result ){
		
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
		
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
			return false;
			
		}
		
		$databaseExists = false;
		$rows  = $result->fetchAll( PDO::FETCH_NUM );
		for( $n = 0; $n < count($rows); $n++){
			if( $rows[$n][0] === $dbName) {
				$databaseExists = true;
			}
		}//next
			
		if( !$databaseExists ){
			$sql_query = "CREATE DATABASE ".$dbName." DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
			$result  = $connection->query( $sql_query );
			echo _logWrap( $result );
		}
*/
		$sql_query = "CREATE DATABASE IF NOT EXISTS ".$dbName." DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci; ";
		$result  = $connection->query( $sql_query );
		if( !$result ){
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
		} else {
			//$msg =  "-- run query: ".$sql_query;
			//echo _logWrap( $msg, "success" );
		}

		$sql_query .= "USE ".$dbName."; ";
		$result  = $connection->query( $sql_query );
		if( !$result ){
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
		}
		
		$sql_query = "CREATE TABLE IF NOT EXISTS `product` (
 `code` int(11) NOT NULL COMMENT 'Артикул',
 `name` varchar(255)  NOT NULL COMMENT 'Наименование',
 `category_id` int(11) COMMENT 'связь с таблицей категорий',
 `description` text COMMENT 'Описание',
 `image_url` text COMMENT 'Изображение (ссылка)',
 PRIMARY KEY (`code`)
);";
		$result  = $connection->query( $sql_query );
		if( !$result ){
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
		}

		$sql_query = "CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) COMMENT 'связь с родительской категорией',
  `name` varchar(255)  NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`category_id`)
); ";
		$result  = $connection->query( $sql_query );
		if( !$result ){
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
		}
		
		$sql_query = "CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL  AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  PRIMARY KEY (`user_id`)
); ";
		$result  = $connection->query( $sql_query );
		if( !$result ){
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
		}
		
		$sql_query = "SELECT * FROM`users`WHERE login='admin'; ";
		$result  = $connection->query( $sql_query );
		if( !$result ){
			$msg =  "-- error, query: ".$sql_query;
			echo _logWrap( $msg, "error" );
			echo "error info:";	
			echo _logWrap( $connection->errorInfo(), "error" );
		} else {
		
			$rows  = $result->fetchAll( PDO::FETCH_NUM );
//echo _logWrap( $rows );
 			if( count( $rows ) === 0 ){
 			
				$sql_query = "
INSERT INTO `users` (`user_id`, `login`, `password`) VALUES 
(1, 'admin', '817a698fc6405f38bcf83fb8f3571e23'); 
";
				$result  = $connection->query( $sql_query );
				if( !$result ){
					$msg =  "-- error, query: ".$sql_query;
					echo _logWrap( $msg, "error" );
					echo "error info:";	
					echo _logWrap( $connection->errorInfo(), "error" );
				}
 			
 			}
			
		}
		
		unset ($connection);
		return true;
		
	} catch( PDOException $exception ) {
		$msg = "-- error connection, ".$exception->getMessage();
		echo _logWrap( $msg, "error" );
	}

	return false;
}//end checkDbMySql()
