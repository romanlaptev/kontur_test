<div class="categories-list">

	<div> 
		<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
		<?php echo CHtml::link(CHtml::encode($data->category_id), array('category/edit', 'id'=>$data->category_id)); ?>
	</div>
	
	<div> 
		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
	</div>

</div>
