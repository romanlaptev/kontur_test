<div class="user-list">

	<div> 
		<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
		<?php echo CHtml::link(CHtml::encode($data->user_id), array('user/edit', 'id'=>$data->user_id)); ?>
	</div>
	
	<div> 
		<b><?php echo CHtml::encode($data->getAttributeLabel('login')); ?>:</b>
		<?php echo CHtml::encode($data->login); ?>
	</div>

</div>
