<style>	
.categories-list .active {
font-weight:bold;
font-size:110%;
background-color:#fff;
}
</style>

	<div class="row">

		<div class="sidebar sidebar-left">
		  <div class="wrapper">
			<div class="panel">
<ul class="categories-list">
<?php
//  $this->widget('zii.widgets.CListView', array(
//   	'dataProvider'=>$categories,
//   	'itemView'=>'categories_list',
//   ));
 	if( isset ($active_category) ){
echo "active category:".$active_category;
echo "<br>";
  	}

$html = "";
$tpl_category = "<li><a href='index.php?r=category/view&id={{category_id}}'>{{name}}</a></li>";
$tpl_category_active = "<li class='active'><a href='index.php?r=category/view&id={{category_id}}'>{{name}}</a></li>";
for( $n = 0; $n < count( $categories ); $n++ ){
 	$record = $categories[ $n ];
 	
 	$html_item = $tpl_category;
  	if( isset ($active_category) ){
 		if( $record["category_id"] === $active_category ){
 			$html_item = $tpl_category_active;
 		}
  	}
 	
  	foreach( $record as $field=>$value ){
  		$html_item = str_replace( "{{".$field."}}", $value,  $html_item);	
  	}//next
 	$html .= $html_item;
}//next
echo $html;
?>	
</ul>
			</div>
			
			<div class="panel pull-right">
<?php
if( !Yii::app()->user->isGuest ){
echo CHtml::link('Clear category list', array('category/clear'));
}
?>			
			</div>
			
		  </div>
		</div><!-- end sidebar -->

		<div class="content">
		  <div class="wrapper">
		  
				<div class="panel">
<ul class="list-related-categories">
<?php
if( isset($related_categories) ){
	if( count($related_categories) > 0 ){
		$html = "";
		$tpl_category = "<li><a href='index.php?r=category/view&id={{category_id}}'>{{name}}</a></li>";
		for( $n = 0; $n < count( $related_categories ); $n++) {
			$record = $related_categories[$n];
			$html_item = $tpl_category;
			foreach( $record as $field=> $value ){
				$html_item = str_replace( "{{".$field."}}", $value,  $html_item);	
			}//next
			$html .= $html_item;
		}//next
		echo $html;
	}
}
?>
</ul>
				</div>
				
				<div class="panel">
<?php
/* @var $this SiteController */

//$this->pageTitle=Yii::app()->name;
//echo "<pre>";
//print_r($products);
//print_r($this);
//echo "</pre>";

  
$tpl_header = "<table>";
$tpl_thead = "<tr><th>{{code}}</th><th>{{name}}</th><th>{{category_id}}</th><th>{{description}}</th><th>{{image_url}}</th></tr>";

$tpl_row = "<tr>";
$tpl_row .= "<td>{{code}}</td><td>{{name}}</td><td>{{category_id}}</td><td>{{description}}</td>";
$tpl_row .= "<td><img src='{{image_url}}' /></td>";
$tpl_row .= "</tr>";

$tpl_footer = "</table>";

$html = "";

$html .= $tpl_header;
$html_thead = $tpl_thead;
foreach( $labels as $key => $label ) {
	$html_thead = str_replace( "{{".$key."}}", $label,  $html_thead);	
}//next
$html .= $html_thead;


for( $n = 0; $n < count( $products); $n++ ){
	$record = 	$products[ $n ];

	$html_row = $tpl_row;
	foreach( $record as $field => $value ){
		$html_row = str_replace( "{{".$field."}}", $value,  $html_row);	
	}//next

	$html .= $html_row;
}//next

$html .= $tpl_footer;

echo $html;


if( !Yii::app()->user->isGuest ){
echo CHtml::link('Clear product catalog', array('site/clear'));
}

?>
				</div>
			</div>
		</div><!-- end content -->
		
	</div><!-- end row -->
