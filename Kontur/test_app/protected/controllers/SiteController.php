<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
// 			// captcha action renders the CAPTCHA image displayed on the contact page
// 			'captcha'=>array(
// 				'class'=>'CCaptchaAction',
// 				'backColor'=>0xFFFFFF,
// 			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}


	public function actionIndex()
	{
		$model = new Product();
		$products =  $model->findAll();
		$labels = $model->attributeLabels();
// echo "<pre>";
// print_r($products);
// print_r($labels);
// echo "</pre>";

 		//$categories=new CActiveDataProvider('Category');
		$model = new Category();
		$categories =  $model->findAll();

  		$this->render('index', array( 
  			'labels'=>$labels, 
  			'products'=>$products,
 			'categories'=>$categories
  		));

	}//end actionIndex()


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionImport()
	{
// 		$line = "Категория 1 / Категория 2 / Категория 3";
// 		$products_category_id = $this->parseCategories( $line );
// 
//  		$line = "Категория 1 / Категория 2 / Категория 4";
//  		$products_category_id = $this->parseCategories( $line );
// 
// 		$line = "Категория 1 / Категория 2 / Категория 5";
// 		$products_category_id = $this->parseCategories( $line );
//echo "products_category_id: ".$products_category_id;
//echo "<br>";
		$this->render('import');
	}//end
	
	
	public function actionUpload()
	{
//echo "<pre>";
//print_r ($_SERVER);
//print_r($_REQUEST);
//print_r($_POST);
//print_r ($_FILES);
//echo "</pre>";
		if ( empty(  $_POST["upload_dir"] ) ){
			return false;
		}
		
		$file_arr =$_FILES["upload_file"];
		if ( empty(  $file_arr["name"] ) ){
//$msg = "Error,  not select XLS file";
//echo $this->_alert($msg, "error");
			return false;
		}
		
		$upload_dir = dirname( $_SERVER['SCRIPT_FILENAME'] ) ."/". $_POST["upload_dir"];
		$this->uploadFile( $upload_dir, $file_arr );
		
		$uploaded_file = $upload_dir."/".$_FILES["upload_file"]["name"];
		if( !file_exists( $uploaded_file ) ) {
			return false;
		 }
		$mime_type = $file_arr["type"];
//[type] => application/vnd.openxmlformats-officedocument.spreadsheetml.sheet		 
		if( ($mime_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') OR 
				($mime_type == 'application/vnd.ms-excel') OR
					($mime_type == 'application/x-excel'))
			{
				//$filename = "upload/import.xlsx";
				$data = $this->getCatalogFromXLS( $uploaded_file );
				
//$data = [];//for test				
				if( count($data) > 0) {
					$this->saveCatalogToDB( $data );
					$this->download_images( $data );
				} else {
//echo "<pre>";
//print_r ($data);
//echo "</pre>";
				}
				
		
			} else {
$msg = "Wrong upload file,  only Excel  format";
echo $this->_alert($msg, "error");
			 }
				 
		if (unlink ( $uploaded_file ))  {
$msg = "-- remove ". $uploaded_file;
echo $this->_alert($msg, "warning");
		  }

		$log = "";
		$this->render('import', array("log" => $log) );
	}//end

	
	private function uploadFile( $upload_dir, $file_arr )
	{

		if ( !is_writable( $upload_dir )){
$msg = "Can not write to <b>" .$upload_dir."</b>";
echo $this->_alert($msg, "error");
			return false;
		}

$msg = "";
		switch ($file_arr['error']){
				case 0:
//$msg .= "<p>UPLOAD_ERR_OK</p>";
//$msg .= "<p>Error code: " . $file_arr['error'] . "</p>";
					if ( is_uploaded_file ($file_arr['tmp_name']) )	{
					
 						$uploaded_file = $upload_dir."/".$file_arr['name'];
						if ( move_uploaded_file( $file_arr['tmp_name'], $uploaded_file ) )	{
//$msg .= $file_arr['name'].", size= ".$file_arr['size']." bytes was uploaded successfully";
//echo $this->_alert($msg, "success");
						} else {
$msg .= $file_arr['name'].", size= ".$file_arr['size']." bytes was not uploaded";
echo $this->_alert($msg, "error");
						}
					}
				break;

 					case 1:
 					case 2:
 					case 3:
 					case 4:
					case 6:
					case 7:
					case 8:
$msg .= "Error code: " . $file_arr['error'];
echo $this->_alert($msg, "error");
					break;

			}// end switch

	}//end

	
	private function getCatalogFromXLS( $filename )
	{
		$objPHPExcel = PHPExcel_IOFactory::load($filename);

		$data = array();
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		//$worksheetTitle = $worksheet->getTitle(); //Лист1Лист2Лист3 

			$highestRow = $worksheet->getHighestRow(); 
			$highestColumn = $worksheet->getHighestColumn(); 
			$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		
			for ($row = 1; $row <= $highestRow; ++$row )	{
//echo "row = ".$row;
//echo "<br>";
				for ($col = 0; $col < $highestColumnIndex; ++$col)
				{
	//echo "col = ".$col;
	//echo "<br>";
					$cell = $worksheet->getCellByColumnAndRow($col, $row);
					$val = $cell->getValue();
					if (!empty( $val ) ) {
						$data[$row-1][$col]= $val;
					}
				}//next
			}//next

		} //next
	
		return $data;
	}//end
	
	private function saveCatalogToDB( $data )
	{
//echo "<pre>";	
//print_r($data);
//print_r($model);
//echo "</pre>";	
//exit;
		for( $n = 1; $n < count($data) ; $n++){//skip first line with field names
			//$n = 3;
			$row = $data[ $n ];
			
			$product = Product::model()->findByPk( $row[2] );//check product code
			if( ! $product ){ // Add new product
//echo "Add new product". $row[2];
//echo "<br/>";	
				$model = new Product();
				$model->code = $row[2];
				$model->name = $row[0];
				
				$model->category_id = $this->parseCategories( $row[1] );
				
				$model->description = $row[3];
				//if( isset( $row[4] ) ) {
					if( !empty( $row[4] ) ) {
						//$model->image_url = $row[4];
						$model->image_url = "images/".basename( $row[4] );
					}
				//}
				
				if( $model->validate() ) {
					$model->save();
					//$model->insert();
				} else {
$msg .= "1.validate error...";
echo $this->_alert($msg, "error");
				}
			}

			if( $product ){ // Update product
//echo "Update new product". $product->code;
//echo "<br/>";	
				$product->name = $row[0];
				
				$product->category_id = $this->parseCategories( $row[1] );

				$product->description = $row[3];
				//if( isset( $row[4] ) ) {
					if( !empty( $row[4] ) ) {
						//$product->image_url = $row[4];
						$product->image_url = "images/".basename( $row[4] );
					}
				//}
				if( $product->validate() ) {
						$product->save();
					} else {
$msg .= "2.validate error...";
echo $this->_alert($msg, "error");
				}
			}
			
		}//next

	}//end


	private function parseCategories( $line)
	{
		$category_names = explode("/", $line);

		$products_category_id= 0;
		$parent_id= 0;
		for( $n = 0; $n < count( $category_names ); $n++){
			$category_names[$n] = trim( $category_names[$n] );

			$model = new Category();
			$category = $model->findByAttributes( array(  'name' =>$category_names[ $n ]) );
			
			if( !$category ){//insert new
				$model->name = $category_names[$n];
				$model->parent_id = $parent_id;
				if( $model->validate() ) {
					$model->save();
					$products_category_id= $model->category_id;
				} else {
					$msg = "validate error...".$model->tableName();
					echo $this->_alert($msg, "error");
				}
				$parent_id= $model->category_id;
			}

			if( $category ){//update
				$category->name = $category_names[$n];
				$category->parent_id = $parent_id;
				if( $category->validate() ) {
					$category->save();
					$products_category_id= $category->category_id;
				} else {
					$msg = "validate error...".$model->tableName();
					echo $this->_alert($msg, "error");
				}
				$parent_id= $category->category_id;
			}
		}//next
//echo "<pre>";
//print_r ($category_names);
//echo "</pre>";
		return $products_category_id;
	}//end
	
	
	private function download_images( $data )
	{
		$images_dir = dirname( $_SERVER['SCRIPT_FILENAME'] ) ."/images";
		for( $n = 1; $n < count($data) ; $n++){//skip first line with field names
			$row = $data[ $n ];
			if( !empty( $row[4] ) ) {
				$img_url = $row[4];
//echo "image url = ".$img_url;
//echo "<br>";
				$this->download_image( $img_url, $images_dir );
			}
		}//next
	}//end download_images()
	
	private function download_image( $img_url, $images_dir )
	{
		//$img_url = "https://kontur-lite.ru/themes/template/images/logo1.png";
		//$images_dir = dirname( $_SERVER['SCRIPT_FILENAME'] ) ."/images";

		if ( !is_dir ( $images_dir ) ){
			//mkdir( $images_dir, 0777 );
			mkdir( $images_dir );
		}
		
		if ( !is_writable( $images_dir )){
$msg = "Can not write to <b>" .$images_dir."</b>";
echo $this->_alert($msg, "error");
			return false;
		}
		
		$file_path = $images_dir ."/".basename($img_url);
		$content = file_get_contents( $img_url );
		if( !$content ){
			return false;
		}
		$num_bytes = file_put_contents( $file_path, $content );
		if( !$num_bytes ){
			return false;
		} else {
$msg = "Saved ".$num_bytes." bytes  to the ".$file_path;
echo $this->_alert($msg, "success");
		}
	}//end download_image()
	
	
	public function actionClear()
	{
		$model = new Product();
		$model->deleteAll();
		$this->redirect(Yii::app()->homeUrl);
	}

	
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	

	private function _alert( $msg, $level){

		switch ($level) {
			case "info":
				return "<div class='alert alert-info'>".$msg."</div>";
			break;
			
			case "warning":
				return "<div class='alert alert-warning'>".$msg. "</div>";
			break;
			
			case "danger":
			case "error":
				return "<div class='alert alert-danger'>".$msg. "</div>";
			break;
			
			case "success":
				return "<div class='alert alert-success'>".$msg. "</div>";
			break;
			
			default:
				return "<div class='alert alert-info'>".$msg. "</div>";
			break;
		}//end switch

	}//end _alert()
	
	
}//end class
