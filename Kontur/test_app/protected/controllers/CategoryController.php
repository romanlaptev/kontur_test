<?php

class CategoryController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionView($id)
	{
		$criteria=new CDbCriteria;
		$criteria->condition='category_id=:category_id';
		$criteria->params=array(':category_id'=>$id);
		$products = Product::model()->findAll($criteria);
		$labels = Product::model()->attributeLabels();
		
		$categories =  Category::model()->findAll();
//echo "<pre>";
//print_r($categories);
//echo "</pre>";		
		$active_category = 0;
		for( $n =0; $n < count($categories); $n++){
			$record = $categories[$n];
			if( $record["category_id"] === $id){
//echo "Mark active category..".$record["category_id"].$record["name"];
//echo "<br>";
				$active_category =  $record["category_id"];
			}//next
		}//next
		
		$related_categories =  Category::model()->findAllByAttributes ( 
			array(  'parent_id' => $id )
		);
// echo "<pre>";
// print_r($related_categories);
// echo "</pre>";
   		$this->render('/site/index', array( 
   			'labels'=>$labels, 
   			'products'=>$products,
  			'categories'=>$categories, //left side-bar, full list of categories
  			'related_categories'=>$related_categories, // list of related categories
  			'active_category' => $active_category
   		));
		//$this->render('index');
	}

	public function actionClear()
	{
		$model = new Category();
		$model->deleteAll();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}//end class