<h1>Edit user settings</h1>
<form name="form_edit_user" action="" method='post' class="form-control">

		<div class="form-group">
				<div><label>Username: </label></div>
				<div><input type="text" name="username" value="{{username}}"></div>
		</div>

		<div class="form-group">
				<div><label>Email: </label></div>
				<div><input type="email" name="email" value="{{email}}"></div>
		</div>

		<div class="form-group">
				<div><label>Login: </label></div>
				<div><input type="text" name="login" value="{{login}}"></div>
		</div>

		<div class="form-group">
				<div><label>Password: </label></div>
				<div><input type="password" name="password"></div>
		</div>

		<div class="form-group">
				<input type='hidden' name="q" value='edit-user'>
				<input type="submit" value="Save changes">
		</div>
</form>

