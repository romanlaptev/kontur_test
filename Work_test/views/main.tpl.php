<?php
//echo "params:<pre>";
//print_r ($params);
//echo "</pre>";
?>
 <!DOCTYPE html>
<html>
<head>
	<title>test page</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/main.css" type="text/css">
</head>
<body>

<div class='header container'>
	<div class='wrapper'>
		<h1>Personal cabinet</h1>
	</div>

	<div id='block-user' class='panel'>
<?php
if ( !empty( $params["login"] ) ) {
	$html = "<h3>{{login}}</h3>";
	$html = str_replace( "{{login}}", $params["login"], $html );
	echo $html;
}
?>
	</div>

</div><!-- end header -->

<div class='menu container'>
	<div class='wrapper'>
		<ul class='pull-left'>
			<li><a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>">Home</a></li>
		</ul>

		<ul class='pull-right'>
<?php
	$html = "<li><a href='?q=reg-form'>Registration</a></li>";
	$html .= "<li><a href='?q=login-form'>Log in</a></li>";

if ( !empty( $params["is_auth"] ) ) {
	$html = "<li><a href='?q=edit-user-form'>Change settings</a></li>";
	$html .= "<li><a href='?q=logout'>Log out</a></li>";
}

echo $html;
?>
		</ul>
	</div>
</div><!-- end menu -->

<div class='main container'>

		<div class='content'>
		  <div class='wrapper'>
				<div class='panel'>
<?php
if ( !empty( $params["tpl_conent"] ) ) {
	echo $params["tpl_conent"];
}
?>
				</div>
			</div>
		</div><!-- end content -->
</div><!-- end main -->

<div class='footer container'>
	<div class='wrapper'>
		<div class='panel'></div>
	</div>
</div><!-- end footer -->


</body>
</html>
