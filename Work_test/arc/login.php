<?php
//ini_set('session.gc_maxlifetime', 3600*24*30);
//ini_set('session.cookie_lifetime', 3600*24*30);
ini_set("session.cookie_lifetime", 0);
session_start();
?>
<html>
<head>
	<title>test page</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">	
	<meta http-equiv="X-UA-Compatible" content="IE=10">
	
	<link rel="stylesheet" href="css/main.css" type="text/css">
</head>
<body>

<?php
//echo "test:<pre>";
//print_r($_REQUEST);
//print_r($_SERVER);
//print_r($_SESSION);
//print_r ($_COOKIE);
//echo "</pre>";
//echo session_save_path();

error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);

$_vars=array();
$_vars["templates"] = defineTemplates();
$_vars["log"] = "";

$_vars["request"] = $_REQUEST;
if( empty($_REQUEST['action'])){
	$_vars["request"]["action"]=""; 
}

if( !isset( $_SESSION['is_auth'] ) ){
	$_SESSION['is_auth'] = false;
}

$_vars["log"] .= "session: <pre>". print_r( $_SESSION, 1). "</pre>";
$_vars["log"] .= "vars: <pre>". print_r( $_vars["request"], 1). "</pre>";

if($_vars["request"]["action"] == "auth"){
	$username = $_vars["request"]["username"];
	$pass = $_vars["request"]["pass"];
	
	if ( verifyUser($username, $pass) ) {
		$_SESSION['is_auth'] = true;
		$_SESSION['user'] = $username;
	}
	
	if( !$_SESSION['is_auth'] ){
		echo "<h1 class='text-danger'>Access denied.</h1>";
	}
}

if ( isset( $_vars["request"]["is_exit"] ) ) {
	session_destroy();
	header("Location:".$_SERVER["SCRIPT_NAME"]);
}

if( !$_SESSION['is_auth'] ){
	echo $_vars["templates"]["formAuth"];
} else {
	initApp();
}


//============================================
function verifyUser($username, $pass) {
	$login = "admin";
	$password = "817a698fc6405f38bcf83fb8f3571e23";//string: m2ster

	if ( ($username == $login) && 
			( md5($pass) == $password ) 
		) {
		return true;
	} else {
		return false;
	}
}//end verifyUser()


function initApp(){
	global $_vars;
	$_vars["username"] = $_SESSION['user'];
	runAction();
}//end initApp


function runAction(){
	global $_vars;
/*	
	if( !empty($_vars['action']) ){
		$_vars["request"]["action"] = $vars['action']; 
	} else {
		$_vars["request"]["action"]=""; 
	}
*/
	echo viewPage();
}//end runAction


function viewPage(){
	global $_vars;
	
	$html = $_vars["templates"]["pageContent"];
	$html = str_replace("{{username}}", $_vars["username"], $html);
	$html = str_replace("{{logoutUrl}}", $_vars["templates"]["logoutUrl"], $html);
	$html = str_replace("{{log}}", $_vars["log"], $html);
	return $html;
}//end viewPage()


function defineTemplates(){

	$tpl["formAuth"] = "<div class='dm-table'>
		<div class='dm-cell'>
			<div class='dm-modal'>
				<div class='center-align'>
	<form name='form_auth' action='' method='post' class='form-control'>
					<div class='panel'>
						<div><label>Username: </label></div>
						<div><input type='text' name='username'></div>
					</div>

					<div class='panel'>
						<div><label>Password: </label></div>
						<div><input type='password' name='pass'></div>
					</div>

					<div class='panel text-center'>
						<input type='hidden' name='action' value='auth'>
						<input type='submit' value='Enter'>
					</div>
	</form>
				</div>
			</div>
		</div>
	</div>
";

	$tpl["pageContent"] = "
<div class='header container'>
	<div class='wrapper'>
		<h1>Personal cabinet</h1>
	</div>
</div><!-- end header -->

<div class='menu container'>
	<div class='wrapper'>
	  <!-- <ul><li><a href='#'>item1</a></li></ul> -->
	</div>
</div><!-- end menu -->

<div class='main container'>

		<div class='content'>
		  <div class='wrapper'>
				<div class='panel'>

		<div id='block-user' class='panel'>
	<h3>{{username}}</h3>
	<div>{{logoutUrl}}</div>
		</div>

				</div>
			</div>
		</div><!-- end content -->


		<div class='log-panel panel'>
			<div class='panel-body'>
				<span class='pull-right'>
					<a id='btn-clear-log' href='#' title='Clear log' class='btn'>x</a>
				</span>
				<div id='log' class='panel-body'>
	{{log}}
				</div>
			</div>
		</div>

</div><!-- end main -->

<div class='footer container'>
	<div class='wrapper'>
		<div class='panel'></div>
	</div>
</div><!-- end footer -->

";

	$tpl["logoutUrl"] = "<a href='?is_exit=1'>logout</a>";

	return $tpl;
}//end defineTemplates()

?>
</body>
</html>
