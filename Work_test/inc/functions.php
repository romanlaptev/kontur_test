<?php

function render_tpl( $tpl_name, $params ) {
	global $config;
	require_once ("views/" .$tpl_name. ".tpl.php");
}//end


function verifyUser( $params ) {
	$result = db_find_user( $params );
//echo _logWrap( $result );

	if( $result["type"] === "error" ){
		$msg = $result["type"].", ".$result["description"];
		echo _logWrap( $msg, "error" );
		return false;
	}

	if( $result["type"] === "success" ){
		$msg = "user verification was successful";
		echo _logWrap( $msg, "success" );
		return true;
	}

	return false;
}//end verifyUser()


function register_user( $form_data ) {
	$result = db_add_user( $form_data );
//echo _logWrap( $result );
	if( $result["type"] === "error" ){
		$msg = $result["type"].", ".$result["description"];
		echo _logWrap( $msg, "error" );
	}
}//end register_user()


function fill_form_edit_user( $params ){
	$p = array(
		"login" => null,
		"tpl_name" => false
	);

	//extend options object $p
	foreach( $params as $key=>$item ){
		$p[ $key ] = $item;
	}//next

	if( $p["tpl_name"] ) {
		$tpl = file_get_contents( $p["tpl_name"] );
	} else {
		$msg = "error, not defined template...";
		echo _logWrap( $msg, "error" );
		return false;
	}

	$result = db_find_user( $p );
//echo _logWrap( $result );
	if( $result["type"] === "error" ){
		$msg = $result["type"].", ".$result["description"];
		echo _logWrap( $msg, "error" );
		return false;
	}

	if( $result["type"] === "success" ){
		//$msg = "user verification was successful";
		//echo _logWrap( $msg, "success" );

		if( count($result["data"]) > 1){
			$msg = "Warning, found ".count($result["data"]). " records for ".$p["login"];
			echo _logWrap( $msg, "warning" );
echo _logWrap( $result );
		}
		$user_data = $result["data"][0];

		$html = $tpl;
		foreach( $user_data as $field=>$value ){
			$html = str_replace( "{{".$field."}}", $value, $html );
		}//next
		return $html;
	}

	return false;

}//end register_user()



function update_user( $form_data ){
	$result = db_update_user( $form_data );
//echo _logWrap( $result );
	if( $result["type"] === "error" ){
		$msg = $result["type"].", ".$result["description"];
		echo _logWrap( $msg, "error" );
		//return false;
	}
	if( $result["type"] === "success" ){
		$msg = "user info has been updated successfully";
		echo _logWrap( $msg, "success" );
	}
	//return false;
}//end update_user()


function _logWrap( $msg, $level = "info"){

	// check API type
	$sapi_type = php_sapi_name();
//echo "php_sapi_name: ". $sapi_type;
//echo "<br/>\n";
//echo "type: ". gettype( $msg);
//echo "<br/>\n";

//-------------
	//$runType = "";
	//if ($sapi_type == 'apache' ||
		//$sapi_type == 'apache2handler' || 
		//$sapi_type == 'cli-server'
	//) {
		$runType = "web";
	//}

	if ( $sapi_type == "cli" ) { $runType = "console"; }
	if ( $sapi_type == "cgi" ) { $runType = "console"; }

//-------------
	if( gettype( $msg) === "array" || 
		gettype( $msg) === "object"
	){
			if ( $runType == "web" ) {
				$out = "<pre>".print_r($msg,1)."</pre>";
				return $out;
			} else {
				$out = print_r($msg,1)."\n";
				return $out;
			}
	}

	if( gettype( $msg) !== "string"){
		return false;
	}

//-------------
	switch ($level) {
		case "info":
			if ( $runType == "web" ) {
				return "<div class='alert alert-info'>".$msg."</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "warning":
			if ( $runType == "web" ) {
				return "<div class='alert alert-warning'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "danger":
		case "error":
			if ( $runType == "web" ) {
				return "<div class='alert alert-danger'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		case "success":
			if ( $runType == "web" ) {
				return "<div class='alert alert-success'>".$msg. "</div>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
		
		default:
			if ( $runType == "web" ) {
				return $msg. "<br/>";
			}
			if ( $runType == "console" ) {
				return $msg."\n";
			}
		break;
	}//end switch

}//end _logWrap()

?>
