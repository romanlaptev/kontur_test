<?php

function db_connect (){
	global $conf;

	$dsn = $conf["db"]["connectionString"];
	$dbUser = $conf["db"]["dbUser"];
	$dbPassword = $conf["db"]["password"];
	$dbName = $conf["db"]["dbName"];
	
	try{
		$connection = new PDO( $dsn, $dbUser, $dbPassword );
		$msg =  "-- ok, connected to the server <b>".$dsn."</b> as ";
		$msg .=  "<b> ".$dbUser."</b>";
		echo _logWrap( $msg, "success" );

		$sql_query = "CREATE DATABASE IF NOT EXISTS ".$dbName." DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci; ";
		$result  = $connection->query( $sql_query );
		runQuery( $connection, $sql_query);

		$sql_query = "USE ".$dbName."; ";
		runQuery( $connection, $sql_query);

		$sql_query = "CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL  AUTO_INCREMENT,
`login` varchar(64) NOT NULL,
`password` varchar(64) NOT NULL,
`email` varchar(64) NOT NULL,
`username` varchar(64) NOT NULL,
PRIMARY KEY (`id`)
); ";
		runQuery( $connection, $sql_query);

		//unset ($connection);
		return $connection;

	} catch( PDOException $exception ) {
		$msg = "-- error connection, ".$exception->getMessage();
		echo _logWrap( $msg, "error" );
	}

}//end db_connect()


function runQuery( $connection, $sql_query ){

	$result  = $connection->query( $sql_query );
	if( !$result ){
		$msg =  "-- error, query: ".$sql_query;
		echo _logWrap( $msg, "error" );
		echo "error info: ". _logWrap( $connection->errorInfo() );
$arr = $connection->errorInfo();
$desc = $arr[2];
		return array(
"type" => "error",
"description" => $desc
		);
	}

	$msg =  "-- run query: ".$sql_query;
	echo _logWrap( $msg, "success" );

	//$rows  = $result->fetchAll( PDO::FETCH_NUM );
	$rows  = $result->fetchAll( PDO::FETCH_ASSOC );
//echo count( $rows );
	if( count( $rows ) > 0 ){
		return array(
"type" => "success",
"data" => $rows
		);
	}

	return array("type" => "success");

}//end runQuery()



function db_add_user( $params ){
	global $conf;

	$connection = db_connect();

	$dbName = $conf["db"]["dbName"];
	$sql_query = "USE ".$dbName."; ";
	runQuery( $connection, $sql_query);

	$tableName = "users";
	$sql_query = "SELECT * FROM ".$tableName." ";
	$sql_query .= "WHERE login='".$params["login"]."' ";
	//$sql_query .= "AND email='".$params["email"]."' ";
	$sql_query .= "; ";
	$result = runQuery( $connection, $sql_query);
//echo count( $result["data"] );
//echo _logWrap( $result  );

	if( !isset( $result["data"] ) ){

		$fields_string = "";
		$values_string = "";
		$n=0;
		foreach( $params as $field=>$value){

			if( $field === "password"){
				$value = md5($value);
			}

			if( $n === 0 ){
				$fields_string .= "`".$field."`";
				$values_string .= "'".$value."'";
			} else {
				$fields_string .= ",`".$field."`";
				$values_string .= ",'".$value."'";
			}
			$n++;
		}//next

		$sql_query = "INSERT INTO `".$tableName."` (".$fields_string.") VALUES (".$values_string."); ";
		$result = runQuery( $connection, $sql_query);
//echo _logWrap( $result );
		unset ($connection);

		return array("type" => "success");

	} else {

		unset ($connection);
		return array(
"type" => "error",
"description" => "user exist..."
		);

	}

}//end db_add_user()


function db_update_user( $params ){
	global $conf;

	$connection = db_connect();

	$dbName = $conf["db"]["dbName"];
	$sql_query = "USE ".$dbName."; ";
	runQuery( $connection, $sql_query);

	$tableName = "users";
	$fields_string = "";
	$n=0;
	foreach( $params as $field=>$value){
		if( !empty( $value) ){
			if( $field === "password"){
				$value = md5($value);
			}
			if( $n === 0 ){
				$fields_string .= "`".$field."` = '".$value."'";
			} else {
				$fields_string .= ", `".$field."` = '".$value."'";
			}
			$n++;
		}
	}//next

	$sql_query = "UPDATE `".$tableName."` SET ".$fields_string." WHERE login='".$params["login"]."'; ";
	$result = runQuery( $connection, $sql_query);
//echo _logWrap( $result );
	unset ($connection);
	return $result;

}//end db_update_user()


function db_find_user( $params ){
	global $conf;

	$connection = db_connect();

	$dbName = $conf["db"]["dbName"];
	$sql_query = "USE ".$dbName."; ";
	runQuery( $connection, $sql_query);

	$tableName = "users";
	$sql_query = "SELECT * FROM ".$tableName." ";
	$sql_query .= "WHERE login='".$params["login"]."' ";
	if( !empty($params["password"]) ){
		$sql_query .= "AND password='".md5( $params["password"] )."' ";
	}
	$sql_query .= "; ";
	$result = runQuery( $connection, $sql_query);
//echo _logWrap( $result  );
//echo count( $result["data"] );
	unset ($connection);

	if( isset( $result["data"] ) ){

		return array(
"type" => "success",
"data" => $result["data"]
		);

	} else {

		return array(
"type" => "error",
"description" => "user <b>".$params["login"]."</b> not found..."
		);

	}

}//end db_find_user()

?>
