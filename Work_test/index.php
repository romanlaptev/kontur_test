<?php
error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);

//ini_set("session.gc_maxlifetime", 60 );//86400 sec
//ini_set("session.cookie_lifetime", 0);
//session_set_cookie_params(0);

session_start();

//echo "test:<pre>";
//print_r($_REQUEST);
//print_r($_SESSION);
//print_r ($_COOKIE);
//print_r($_SERVER);
//echo "</pre>";
//echo session_save_path();

$conf = require_once("config.php");
require_once dirname(__FILE__)."/inc/db.php";
require_once dirname(__FILE__)."/inc/functions.php";


$params = array();
if( !empty($_REQUEST['q']) ){

	switch ( $_REQUEST['q'] ) {

		case "reg-form":
			$params["tpl_conent"] = file_get_contents( "views/form-reg.tpl.php" );
		break;
		case "reg":
			$form_data = array(
				"username" => $_REQUEST["username"],
				"email" => $_REQUEST["email"],
				"login" => $_REQUEST["login"],
				"password" => $_REQUEST["password"]
			);
			register_user( $form_data );
		break;

		case "login-form":
			$params["tpl_conent"] = file_get_contents( "views/form-login.tpl.php" );
		break;
		case "login":
			$arg = array(
				"login" => $_REQUEST["login"],
				"password" => $_REQUEST["password"]
			);
			if ( verifyUser( $arg ) ) {
				$_SESSION['is_auth'] = true;
				$_SESSION['login'] = $_REQUEST["login"];
			}
		break;

		case "edit-user-form":
			//$params["tpl_conent"] = "views/form-edit-user.tpl.php";
			$arg = array(
				"login" => $_SESSION["login"],
				"tpl_name" => "views/form-edit-user.tpl.php"
			);
			$params["tpl_conent"] = fill_form_edit_user( $arg );
		break;
		case "edit-user":
			$form_data = array(
				"username" => $_REQUEST["username"],
				"email" => $_REQUEST["email"],
				"login" => $_REQUEST["login"],
				"password" => $_REQUEST["password"]
			);
			update_user( $form_data );
		break;

		case "logout":
			//$params["is_auth"] = false;
			//$params["login"] = null;
			session_destroy();
			header("Location:".$_SERVER["SCRIPT_NAME"]);
		break;

	}// end switch
}

if( !isset( $_SESSION['is_auth'] ) ){
	$_SESSION['is_auth'] = false;
}

if( $_SESSION['is_auth'] ){
	$params["login"] = $_SESSION['login'];
	$params["is_auth"] = true;
}
render_tpl( "main", $params );

?>
